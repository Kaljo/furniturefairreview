import React from 'react';
import {Div, Button, Input} from './../basics/components';
import '../../css/loginPage.css';
import '../../css/modal.css';
import Popup from 'reactjs-popup';
import {connect} from 'react-redux';
import {post} from '../services/service';
import {LOGIN_RESPONSE} from '../../redux/actions/userActions';

const ContactUs = () => (
	<Popup trigger={<div className="contact-us">Contact Us?</div>} modal>
		{(close) => (
			<Div className="modal">
				<Div className="header">Contact Us</Div>
				<Div className="content">
					<Input placeholder="Firm name" />
					<Input placeholder="Contact info" />
					<Input placeholder="Problem description" />
				</Div>
				<Div className="actions">
					<Button onClick={close}>Send</Button>
				</Div>
			</Div>
		)}
	</Popup>
);

class LoginPage extends React.Component {
	state = {
		email: '',
		password: '',
		error: ''
	};

	login = () => {
		const {email, password} = this.state;
		post('signin', {email, password}, false, this.loginResponse, this.loginError);
	};

	loginResponse = (data) => {
		this.props.loginResponse(data);
		this.setState({
			error: ''
		});
	};

	loginError = () => {
		this.setState({
			error: 'Wrong username or password',
			email: '',
			password: ''
		});
	};

	setInput = (e, field) => {
		this.setState({
			[field]: e.target.value
		});
	};

	render() {
		const {email, password} = this.state;
		return (
			<Div className="login-container">
				<Div className="login-title">Furniture Fair</Div>
				<Div className="login-form">
					<Div className="login-text">Login</Div>
					<Div className="form">
						<Input placeholder="email" value={email} onChange={(e) => this.setInput(e, 'email')} />
						<Input placeholder="password" value={password} onChange={(e) => this.setInput(e, 'password')} type="password" />
						<Button label="Sign in" onClick={this.login} />
						<Div className="login-error">
							<Div className="error-text">{this.state.error || ''}</Div>
							<ContactUs />
						</Div>
					</Div>
				</Div>
			</Div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		loginResponse: (response) => dispatch({type: LOGIN_RESPONSE, response})
	};
};

export default connect(
	null,
	mapDispatchToProps
)(LoginPage);
