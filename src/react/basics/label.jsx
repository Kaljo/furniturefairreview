import React, {PureComponent} from 'react';

export default class Label extends PureComponent {
	render() {
		return <label {...this.props}>{this.props.children}</label>;
	}
}
