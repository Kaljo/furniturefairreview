import React, {PureComponent} from 'react';

export default class Image extends PureComponent {
	render() {
		return (
			<img alt={this.props.alt} {...this.props}>
				{this.props.children}
			</img>
		);
	}
}
