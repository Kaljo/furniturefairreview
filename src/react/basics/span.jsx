import React, {PureComponent} from 'react';

export default class Span extends PureComponent {
	render() {
		return <span {...this.props}>{this.props.children}</span>;
	}
}
