import React, {Component} from 'react';

class Input extends Component {
	render() {
		return (
			<>
				{this.props.label && <label>{this.props.label}</label>}
				<input {...this.props} />
			</>
		);
	}
}

export default Input;
