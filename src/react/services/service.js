import axios from 'axios';
// import store from '../redux/store';
// import {addPopupMessage, logoutUser} from '../redux/actions';

export const getUrl = (url) => {
	return `https://furniture-fair-api.herokuapp.com/${url}`;
};

export const POST_FILE_HEADERS = {
	headers: {
		'Content-Type': 'multipart/form-data'
	}
};

export const POST_IMAGE_HEADERS = {
	headers: {
		'Content-Type': 'multipart/form-data'
	},
	responseType: 'blob'
};

export const APPLICATION_JSON = {
	header: {
		'Content-Type': 'application/json'
	}
};

export const get = (url, withCredentials = false, responseCallback, errorCallback) => {
	axios
		.get(getUrl(url), {withCredentials: withCredentials})
		.then((response) => {
			responseCallback && responseCallback(response.data);
		})
		.catch((error) => showErrorMessage(error, errorCallback));
};

export const post = (url, request, withCredentials = false, responseCallback, errorCallback) => {
	axios
		.post(getUrl(url), request, {withCredentials: withCredentials})
		.then((response) => responseCallback && responseCallback(response.data))
		.catch((error) => showErrorMessage(error, errorCallback));
};

export const postFile = (url, name, file, withCredentials = false, headers, responseCallback, errorCallback) => {
	let request = new FormData();
	request.append(name, file);
	axios
		.post(getUrl(url), request, {...headers, withCredentials: withCredentials})
		.then((response) => {
			responseCallback && responseCallback(response.data);
		})
		.catch((error) => showErrorMessage(error, errorCallback));
};

const showErrorMessage = ({message, response}, errorCallback) => {
	// if (response && response.status === 403) {
	// 	store.dispatch(logoutUser());
	// } else {
	// 	store.dispatch(addPopupMessage(message || 'Platform is broken, please try again'));
	// }
	if (errorCallback) {
		errorCallback();
	}
};
