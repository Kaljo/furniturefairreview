import {createStore} from 'redux';
import throttle from 'lodash/throttle';
import reducers from './reducers/reducers';

const storage = localStorage;

/** Load redux state from locale storage (fix for refreshing page problem) */
const loadState = () => {
	try {
		const serializedState = storage.getItem('state');
		if (serializedState === null) {
			return undefined;
		}
		return JSON.parse(serializedState);
	} catch (error) {
		return undefined;
	}
};

/** Save redux state to locale storage (fix for refreshing page problem) */
const saveState = (state) => {
	try {
		storage.setItem('state', JSON.stringify(state));
	} catch (error) {
		console.log(error);
	}
};

/** Creating redux store with reducers */
const store = createStore(reducers, loadState(), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

store.subscribe(
	throttle(() => {
		saveState(store.getState());
	}, 1000)
);

export default store;
