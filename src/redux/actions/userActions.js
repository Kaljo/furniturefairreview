export const LOGIN_RESPONSE = 'LOGIN_RESPONSE';

export const loginResponse = (response) => ({type: LOGIN_RESPONSE, response});
