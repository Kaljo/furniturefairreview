import {LOGIN_RESPONSE} from '../actions/userActions';

const userReducer = (
	state = {
		token: '',
		userId: '',
		type: ''
	},
	action
) => {
	switch (action.type) {
		case LOGIN_RESPONSE:
			return {...state, ...action.response};
		default:
			return state;
	}
};

export default userReducer;
