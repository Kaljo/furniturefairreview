import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './redux/store';
import LoginPage from './react/pages/loginPage';
import Routes from './utils/routes';

class App extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<Router>
					<Switch>
						<Route exact path={Routes.ROOT} component={LoginPage} />
						<Route exact path={Routes.LOGIN} component={LoginPage} />
					</Switch>
				</Router>
			</Provider>
		);
	}
}

export default App;
